import 'package:agillizapp_erp/src/config/custom_colors.dart';
import 'package:agillizapp_erp/src/pages/auth/sign_in_screen.dart';
import 'package:agillizapp_erp/src/pages/bank/bank_persist_screen.dart';
import 'package:agillizapp_erp/src/services/bank_service.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Get.putAsync(() => BankService().init());
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'AgillizApp - ERP',
      routes: {'/bank_persist_page': (context) => BankPersistScreen()},
      //home: const AgillizAppScreen(title: 'Separando as classes'),
      home: const SignInScreen(),
      theme: ThemeData(
        primarySwatch: Colors.purple,
        colorScheme:
            ColorScheme.fromSeed(seedColor: CustomColors.customSwatchColor),
        useMaterial3: true,
      ),
    );
  }
}
