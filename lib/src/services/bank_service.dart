// ignore_for_file: depend_on_referenced_packages

import 'dart:async';
import 'package:agillizapp_erp/src/models/bank_model.dart';
import 'package:get/get.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class BankService extends GetxService {
  late Database db;

  Future<BankService> init() async {
    db = await _getDatabase();
    //criar banco de teste
    // final bank = BankModel(
    //   codigo: 't1',
    //   nome: 't1',
    //   url: 'c1',
    // );
    // await save(bank);
    //await getAll();
    return this;
  }

  Future<Database> _getDatabase() async {
    // Recupera pasta da aplicacao
    var databasesPath = await getDatabasesPath();
    // Recupera caminho da database e excluir database
    // String path = join(databasesPath, 'notes.db');
    // descomente o await abaixo para excluir a base de dados do caminho
    // recuperado pelo path na inicializacao
    // await deleteDatabase(path);
    // Retorna o banco de dados aberto
    return db = await openDatabase(
      join(databasesPath, 'agillizapp.db'),
      onCreate: (db, version) {
        return db.execute('''
          CREATE TABLE BANCO (
            id INTEGER PRIMARY KEY,
            codigo TEXT,
            nome TEXT,
            url TEXT
          )
          ''');
      },
      version: 1,
    );
  }

  // recuperar todas as notas
  Future<List<BankModel>> getAll() async {
    final result = await db.rawQuery('SELECT * FROM BANCO ORDER BY id');
    print(result);
    return result.map((json) => BankModel.fromJson(json)).toList();
  }

  //criar nova nota
  Future<BankModel> save(BankModel bank) async {
    final id = await db.rawInsert(
        'INSERT INTO BANCO (codigo, nome, url) VALUES (?,?,?)',
        [bank.codigo, bank.nome, bank.url]);

    print(id);
    return bank.copy(id: id);
  }

  //atualizar nota
  Future<BankModel> update(BankModel bank) async {
    final id = await db.rawUpdate(
        'UPDATE BANCO SET codigo = ?, nome = ?, url = ? WHERE id = ?',
        [bank.codigo, bank.nome, bank.url, bank.id]);

    print(id);
    return bank.copy(id: id);
  }

  //excluir nota
  Future<int> delete(int bankId) async {
    final id = await db.rawDelete('DELETE FROM BANCO WHERE id = ?', [bankId]);

    print(id);
    return id;
  }

  //fechar conexao com o banco de dados, funcao nao usada nesse app
  Future close() async {
    db.close();
  }
}
