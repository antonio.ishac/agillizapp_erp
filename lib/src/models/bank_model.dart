import 'dart:convert';

List<BankModel> noteFromJson(String str) =>
    List<BankModel>.from(json.decode(str).map((x) => BankModel.fromJson(x)));

String noteToJson(List<BankModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class BankModel {
  BankModel({
    this.id,
    required this.codigo,
    required this.nome,
    this.url,
  });

  int? id;
  String codigo;
  String nome;
  String? url;

  factory BankModel.fromJson(Map<String, dynamic> json) => BankModel(
        id: json['id'],
        codigo: json['codigo'],
        nome: json['nome'],
        url: json['url'],
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'codigo': codigo,
        'nome': nome,
        'url': url,
      };

  BankModel copy({
    int? id,
    String? codigo,
    String? nome,
    String? url,
  }) =>
      BankModel(
        id: id ?? this.id,
        codigo: codigo ?? this.codigo,
        nome: nome ?? this.nome,
        url: url ?? this.url,
      );
}
