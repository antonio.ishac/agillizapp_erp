import 'package:agillizapp_erp/src/config/custom_colors.dart';
import 'package:agillizapp_erp/src/pages/bank/controller/bank_controller.dart';
import 'package:agillizapp_erp/src/pages/components/custom_text_field.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BankPersistScreen extends GetView<BankController> {
  @override
  final controller = Get.find<BankController>();
  BankPersistScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: CustomColors.customSwatchColor,
      body: SingleChildScrollView(
        child: SizedBox(
          height: size.height,
          width: size.width,
          child: Stack(
            children: [
              Column(
                children: [
                  const Expanded(
                    child: Center(
                      child: Text(
                        'Manutenção de Bancos',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 26,
                        ),
                      ),
                    ),
                  ),

                  //Formulário
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 32,
                      vertical: 40,
                    ),
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.vertical(
                        top: Radius.circular(25),
                      ),
                    ),
                    child: Form(
                      key: controller.formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          CustomTextField(
                            icon: Icons.code,
                            label: 'Código',
                            controller: controller.codigoController,
                            focusNode: controller.codigoFocusNode,
                            textInputType: TextInputType.text,
                            onSaved: (value) {
                              controller.codigoFocusNode.requestFocus();
                            },
                            validator: (value) {
                              return controller.validarCodigo(value);
                            },
                          ),
                          CustomTextField(
                            icon: Icons.food_bank,
                            label: 'Nome',
                            controller: controller.nomeController,
                            focusNode: controller.nomeFocusNode,
                            textInputType: TextInputType.text,
                            onSaved: (value) {
                              controller.nomeFocusNode.requestFocus();
                            },
                            validator: (value) {
                              return controller.validarNome(value);
                            },
                          ),
                          CustomTextField(
                              icon: Icons.web,
                              label: 'Url',
                              controller: controller.urlController,
                              focusNode: controller.urlFocusNode,
                              textInputType: TextInputType.text,
                              onSaved: (value) {
                                controller.urlFocusNode.requestFocus();
                              }),
                          SizedBox(
                            height: 50,
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.green,
                                foregroundColor: Colors.white,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(18),
                                ),
                              ),
                              onPressed: () {
                                controller.editMode();
                                if (controller.loading.value == true) {
                                  Get.dialog(
                                    const Center(
                                      child: CircularProgressIndicator(),
                                    ),
                                  );
                                }
                              },
                              child: const Text(
                                'Salvar',
                                style: TextStyle(fontSize: 18),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Positioned(
                top: 10,
                left: 10,
                child: SafeArea(
                  child: IconButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    icon: const Icon(Icons.arrow_back_ios),
                    color: Colors.white,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
