import 'package:agillizapp_erp/src/config/custom_colors.dart';
import 'package:agillizapp_erp/src/pages/bank/controller/bank_controller.dart';
import 'package:agillizapp_erp/src/pages/bank/provider/bank_provider.dart';
import 'package:agillizapp_erp/src/pages/bank/repositorie/bank_repository.dart';
import 'package:agillizapp_erp/src/pages/components/custom_text_field.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BankListScreen extends GetView<BankController> {
  BankListScreen({super.key});

  @override
  final controller = Get.put(BankController(BankRepository(BankProvider())));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.customSwatchColor,
      appBar: AppBar(
        backgroundColor: CustomColors.customSwatchColor,
        foregroundColor: Colors.white,
        title: const Text('Lista de Bancos'),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          children: [
            // const SizedBox(
            //   height: 20,
            // ),
            Card(
              child: Padding(
                padding: const EdgeInsets.only(top: 13, left: 10, right: 10),
                child: CustomTextField(
                  icon: Icons.search,
                  label: 'Buscar',
                  textInputType: TextInputType.text,
                  onChanged: (value) => controller.filterBank(value),
                ),
              ),
            ),
            // const SizedBox(
            //   height: 20,
            // ),
            Expanded(
              child: Obx(
                () => ListView.builder(
                  itemCount: controller.bankList.length,
                  itemBuilder: (context, index) {
                    return Card(
                      //elevation: 5,
                      color: Colors.white,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 13),
                        child: ListTile(
                          title: Text(controller.bankList[index].nome),
                          contentPadding:
                              const EdgeInsets.symmetric(horizontal: 20),
                          trailing: PopupMenuButton(
                            padding: const EdgeInsets.only(left: 20),
                            itemBuilder: (ctx) => [
                              PopupMenuItem(
                                child: const Text("Deletar"),
                                onTap: () {
                                  Get.defaultDialog(
                                      title: 'Excluir Banco',
                                      middleText:
                                          'Excluir o Banco ${controller.bankList[index].nome}?',
                                      textCancel: 'Voltar',
                                      onConfirm: () {
                                        controller.deleteBank(
                                            controller.bankList[index].id!);
                                        if (controller.loading.value == true) {
                                          Get.dialog(
                                            const Center(
                                                child:
                                                    CircularProgressIndicator()),
                                          );
                                        }
                                      });
                                },
                              ),
                              PopupMenuItem(
                                child: const Text("Editar"),
                                onTap: () {
                                  controller
                                      .editBank(controller.bankList[index]);
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.green.shade100,
        foregroundColor: Colors.green.shade900,
        child: const Icon(Icons.add),
        onPressed: () {
          controller.addBank();
        },
      ),
    );
  }
}
