import 'package:agillizapp_erp/src/models/bank_model.dart';
import 'package:agillizapp_erp/src/pages/bank/provider/bank_provider.dart';

class BankRepository {
  final BankProvider api;

  BankRepository(this.api);

  getAll() {
    return api.getAll();
  }

  save(BankModel bank) {
    return api.save(bank);
  }

  update(BankModel bank) {
    return api.update(bank);
  }

  delete(int id) {
    return api.delete(id);
  }
}
