import 'package:agillizapp_erp/src/models/bank_model.dart';
import 'package:agillizapp_erp/src/pages/bank/bank_persist_screen.dart';
import 'package:agillizapp_erp/src/pages/bank/repositorie/bank_repository.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BankController extends GetxController {
  final BankRepository repository;
  BankController(this.repository);

  //variavel do titulo
  String titulo = '';

  //variavel que controla o loading
  RxBool loading = false.obs;

  //variaveis da lista de bancos
  final bankList = <BankModel>[].obs;

  //variaveis do form
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  TextEditingController codigoController = TextEditingController();
  TextEditingController nomeController = TextEditingController();
  TextEditingController urlController = TextEditingController();

  FocusNode codigoFocusNode = FocusNode();
  FocusNode nomeFocusNode = FocusNode();
  FocusNode urlFocusNode = FocusNode();

  //recuperar os bancos para apresentar na tela inicial
  @override
  void onReady() async {
    super.onReady();
    getAll();
  }

  //recuperar todos os bancos
  getAll() {
    loading(true);
    repository.getAll().then((data) {
      bankList.value = data;
      loading(false);
    });
  }

  //tratar formulario para inclusao de um banco
  addBank() {
    formKey.currentState?.reset();

    codigoController.text = '';
    nomeController.text = '';
    urlController.text = '';

    titulo = 'Incluir Banco';
    Get.to(() => BankPersistScreen());
  }

  //tratar formulario para edicao de um banco passando id via arguments
  editBank(BankModel bank) {
    codigoController.text = bank.codigo;
    nomeController.text = bank.nome;
    urlController.text = bank.url!;

    titulo = 'Editar Banco';
    Get.to(() => BankPersistScreen(), arguments: bank.id);
  }

  //verificar se o formulario esta validado sem erros
  //e se um id do banco eh enviado para a tela de edicao
  //o banco sera atualizado, caso contrario sera criado um novo banco
  editMode() {
    codigoFocusNode.unfocus();
    if (formKey.currentState!.validate()) {
      loading(true);
      if (Get.arguments == null) {
        saveBank();
      } else {
        updateBank();
      }
    }
  }

  //salvar um novo banco
  saveBank() async {
    final bank = BankModel(
      codigo: codigoController.text.trim(),
      nome: nomeController.text.trim(),
      url: urlController.text.trim(),
    );
    repository.save(bank).then((data) {
      loading(false);
      refreshBankList();
    });
  }

  //atualizar um banco existente cujo id eh recuperado via arguments
  updateBank() async {
    final note = BankModel(
      id: Get.arguments,
      codigo: codigoController.text.trim(),
      nome: nomeController.text.trim(),
      url: urlController.text.trim(),
    );
    repository.update(note).then((data) {
      loading(false);
      refreshBankList();
    });
  }

  //excluir banco via id
  deleteBank(int noteId) async {
    loading(true);
    repository.delete(noteId).then((data) {
      loading(false);
      refreshBankList();
    });
  }

  void filterBank(String name) {
    List<BankModel> results = [];
    if (name.isEmpty) {
      results = getAll();
    } else {
      results = bankList
          .where((element) => element.nome
              .toString()
              .toLowerCase()
              .contains(name.toLowerCase()))
          .toList();
    }
    bankList.value = results;
  }

  // atualizar lista de bancos apos uma inclusao, alteracao ou exclusao
  refreshBankList() {
    // recuperar lista de notas
    getAll();
    //fechar dialog
    Get.back();
    //voltar para a lista de notas
    Get.back();
  }

  // validar campo codigo
  validarCodigo(String? value) {
    if (value == null || value.isEmpty) {
      return 'Preencha o campo Código.';
    }
    return null;
  }

  //validar campo nome
  validarNome(String? value) {
    if (value == null || value.isEmpty) {
      return 'Preencha o campo nome.';
    }
    return null;
  }
}
