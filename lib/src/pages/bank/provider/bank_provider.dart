import 'package:agillizapp_erp/src/models/bank_model.dart';
import 'package:agillizapp_erp/src/services/bank_service.dart';
import 'package:get/get.dart';

class BankProvider {
  final bankService = Get.find<BankService>();

  Future<List<BankModel>> getAll() async {
    //descomente a linha abaixo para simular um tempo maior de resposta
    // await Future.delayed(Duration(seconds: 2));
    return await bankService.getAll();
  }

  Future<BankModel> save(BankModel bank) async {
    //descomente a linha abaixo para simular um tempo maior de resposta
    await Future.delayed(const Duration(seconds: 2));
    return await bankService.save(bank);
  }

  Future<BankModel> update(BankModel bank) async {
    //descomente a linha abaixo para simular um tempo maior de resposta
    await Future.delayed(const Duration(seconds: 2));
    return await bankService.update(bank);
  }

  Future<int> delete(int bankId) async {
    //descomente a linha abaixo para simular um tempo maior de resposta
    await Future.delayed(const Duration(seconds: 2));
    return await bankService.delete(bankId);
  }
}
