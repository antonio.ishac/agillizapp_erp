import 'package:agillizapp_erp/src/config/custom_colors.dart';
import 'package:agillizapp_erp/src/pages/components/custom.drawer.dart';
import 'package:flutter/material.dart';

class AgillizAppScreen extends StatefulWidget {
  const AgillizAppScreen({super.key, required this.title});

  final String title;

  @override
  State<AgillizAppScreen> createState() => _AgillizAppScreenState();
}

class _AgillizAppScreenState extends State<AgillizAppScreen> {
  TextStyle commonStyle() => const TextStyle(
        fontSize: 17,
        fontWeight: FontWeight.w500,
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: CustomColors.customSwatchColor,
        foregroundColor: Colors.white,
        title: const Text('Home'),
        centerTitle: true,
      ),
      drawer: const CustomDrawer(),
      backgroundColor: Colors.grey[200],
      body: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              'Página principal',
              style: commonStyle(),
            ),
          ],
        ),
      ),
    );
  }
}
